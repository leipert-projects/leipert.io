---
slug: kunzemann
title: "Gutschein"
subtitle: <h2>1000 Gramm Filament</h2>
date: 2020-05-16T11:45:35+02:00
language: de
---

Das sind ungefähr 50 Ruinenteile aus Ulvheim:

- <https://www.thingiverse.com/thing:3274436>
- <https://www.thingiverse.com/thing:3384494>

Oder X-beliebiges. Hier sind andere Inspirationen:

- <https://www.thingiverse.com/Terrain4Print/designs>
- <https://www.thingiverse.com/thing:3756396>
- <https://www.thingiverse.com/dutchmogul/designs>
- <https://www.thingiverse.com/groups/tabletopgamers/things>
- <https://www.thingiverse.com/groups/wargame/things>
- <https://www.thingiverse.com/groups/wargaming-terrain/things>

Von Steven & Lukas
