---
slug: buchhaltung-teil-1-rsu
title: "Buchhaltung Mitarbeiteranteile"
subtitle: '<h2>Teil 1: Restricted Stock Units</h2>'
date: 2023-01-16T23:55:22Z
language: de
expose: true
summary: "Teil 1: Restricted Stock Units"
---

{{< accounting/disclaimer >}}

## Einleitung {.startofdoc}

{{< toc >}}

Ziel der Artikelserie: Eine möglichst anfängerfreundliche Einleitung zur
Buchhaltung von Mitarbeiteranteilen an US-Firmen mit [beancount]. Mit Hilfe
dieser Artikelserie und beancount schafft es jeder sich einen Überblick über
seine Mitarbeiteranteile in weniger als zwei Stunden zu schaffen. Es gibt vier
Teile:

{{% snippet "snippets/buchhaltung_series.md" %}}

Ein Grundverständnis für RSUs, ESPP und Stock Options schadet nicht, aber muss
auch nicht vorhanden sein. Eine detaillierte Einleitung hierzu kann man auch
[in Sujeevan's Blog lesen][svji]. Die Artikel beziehen sich auf die Situation in
Deutschland. Es sollte aber gut übertragbar auf andere Länder sein.

## Warum eigentlich?

Es gibt mehrere Gründe für eine korrekte Buchhaltung, für mich sticht heraus:

- Es erlaubt zu prüfen, ob Gehaltsabrechnungen und die Anzahl der ausgegebenen
  Anteile im Depot korrekt sind.
- Berichte auf den Daten erlaubt die Rendite für das ESPP zu ermitteln und zu
  sehen ob der Steuerberater vielleicht Quatsch zusammengerechnet hat. Der
  US-Broker übermittelt keine Daten über Kapitalerträge an das deutsche
  Finanzamt.
- Bei RSU und ESPP Programmen werden mehrmals im Jahr Anteile ausgegeben und
  US-Broker erlauben nur bedingt eine gute Übersicht mit Hinblick auf
  Gegebenheiten in Deutschland. Manchmal ist es schlichtweg beim Broker nicht
  möglich, Anteile nach First-In-First-Out-Prinzip ("FIFO") zu verkaufen

Auf das Warum folgt das "Wie". Ursprünglich hatte ich hier eine lange Einleitung
zur doppelten Buchführung und [beancount] geplant. Aber das
Wirtschaftsinformatik-Studium hat mich damals im Buchhaltungskurs verloren und
es soll ja niemanden hier genau so ergehen.

Daher: Einfach loslegen und Probleme lösen.

## Restricted Stock Units (RSUs)

Restricted Stock Units (RSUs) sind eine Vergütungsmethode, bei der der
Arbeitnehmer Firmenanteile bekommt. Sie sind quasi als Teil der kompletten
Vergütung anzusehen und im Prinzip "Naturalien", die man vom Arbeitgeber
bekommt. Für den Arbeitgeber ist dies relativ günstig, da der "Markt" den
Arbeitnehmer bezahlt.

Im Grunde gibt es bei RSUs drei Events von Bedeutung:

1. _Grant_: Der Arbeitgeber verspricht dem Mitarbeiter über einen Zeitraum X,
   eine Anzahl von Firmenanteilen zu überschreiben
2. _Vesting_: In gewissen Intervallen erhält der Mitarbeiter Anteile in sein
   Depot gebucht. Dieses Depot ist oft bei einem Broker/Bank, die vom
   Arbeitgeber ausgesucht wurde.
3. _Verkauf_ von Anteilen. Dies geschieht teilweise automatisch um zum Beispiel
   anfallende Steuern zu begleichen oder halt manuell, wenn man Gewinne
   realisieren möchte.

Beispiel: Der Mitarbeiter erhält einen Grant _RSU-4241_ über {{< bc "4800"
"ECORP" >}} Anteile des Arbeitgeber "E Corp". Über 4 Jahre "vesten" alle drei
Monate {{< bc "300" "ECORP" >}} Anteile.

### Grant

Der Grant ist einfach in beancount zu modellieren:

```beancount
; Need to open an account before use
2022-02-01 open Assets:US:ECORP:RSU-4241

2022-02-01 * "RSU Grant RSU-4241"
  Assets:US:ECORP:RSU-4241    4800 ECORP.UNVEST { 0 USD }
```

Im Beispiel gibt es eine Transaktion mit nur einem Konto (in beancount "Account"
genannt). Die {{< bc "4800" "ECORP.UNVEST" >}} Anteile werden zu je {{< bc "0"
"USD" >}} gebucht, denn solange die Anteile nicht im Depot sind, haben sie ja
keinen Wert und sind "Fantasie".

Dieses _Assets_ Konto wird nur genutzt um einen Überblick über die Anteile zu
halten, die noch nicht "gevestet" sind. Es ist also optional und wer weniger in
beancount aufschreiben möchte, kann dies auslassen..

{{% side_note "Was ist ein Konto in beancount?" %}}

In beancount nutzt man viele Konten. Sehr viele. Es gibt vier Haupttypen:

- Assets (`+`): Das was man hat.
- Liabilities (`-`): Das was man schuldet.
- Income (`-`): Wo es herkommt. (Achtung: Vorzeichen invers, also Einkommen ist
  "negativ")
- Expenses (`+`): Wo es hingeht. (Achtung: Vorzeichen invers, also Ausgaben sind
  "positiv")

Alle Kontennamen fangen mit einem dieser Typen an. Der Rest des Kontonamen ist
gleichzeitig eine Hierarchie (durch `:` getrennt). {{< bc
"Assets:DE:Postbank:Giro" >}} ist zum Beispiel ein Girokonto bei der Postbank in
Deutschland. Die Hierarchie erlaubt es beancount in Berichten Dinge
aufzusummieren, sodass ich trotz detailliertem Tracking von Ausgaben {{< bc
"Expenses:Groceries:Beer" >}} und {{< bc "Expenses:Groceries:Tomatoes" >}}
trotzdem den Gesamtüberblick über alle Ausgaben für Lebensmittel bewahre.

Mehr Details: [beancount Dokumentation: The Double-Entry Counting Method][decm]

{{%/ side_note %}}

### Vesting

Etwas Zeit ist vergangen und es werden Anteile in das Depot gebucht. In unserem
Beispiel ist dieses bei "MyBroker", in Realität ist es sicher Schwab oder
E*Trade. An der Transaktion sind deutlich mehr Konten beteiligt:

```beancount
2022-03-15 * "RSU Vesting March"
  Assets:US:ECORP:RSU-4241                 -300 ECORP.UNVEST { }
  Assets:US:MyBroker:ECORP                  300 ECORP { 52.50 USD }
    lot: "RSU-4241"
  Income:US:ECORP:PecAdvantage:RSU       -15750 USD
  Assets:US:ECORP:RSU:TaxWithholding    8032.50 USD
  Liabilities:US:MyBroker:TaxAndStuff  -8032.50 USD
```

Vom _Grant_-Konto {{< bc "Assets:US:ECORP:RSU-4241">}}, werden {{< bc "300"
"ECORP.UNVEST" >}} gebucht. Wenn man den _Grant_ nicht modelliert hat, einfach
die Zeile weglassen.

Auf das Konto beim Broker werden {{< bc "300" "ECORP" >}} zu je {{< bc "52.50"
"USD" >}} gebucht. Das war der Aktien-Kurs von {{< bc "ECORP" >}} an diesem Tag.
Unser Broker erlaubt uns aus verschiedenen "Chargen" (_Lots_) zu verkaufen. Um
später nachzuvollziehen welche Shares wir im Broker wie verkauft haben, gibt es
hier den `lot:` Metadaten-Eintrag.

Diese Anteile haben einen Gegenwert {{< bc "15750" "USD" >}}. Dieser Gegenwert
wird auf ein "Einkommenskonto" gebucht. In Deutschland ist dieser Gegenwert ein
geldwerter Vorteil (Pecuniary Advantage) und wird üblicherweise während der
Gehaltsauszahlung in einem Folgemonat abgerechnet.

Um die Kosten (Steuern, Krankenkasse, etc.) für den geldwerten Vorteil zu
begleichen, hat der Arbeitgeber den Broker beauftragt Geld einzubehalten. Hier
sind es 51% des geldwerten Vorteils (im Beispiel {{< bc "8032.50" "USD" >}}),
die an den Arbeitgeber weitergegeben wurden. Exakt diese Summe schulden wir nun
dem Broker 😱.

Es ist eine ganze Menge passiert, aber am Ende sind auf dem Depot {{< bc "300"
"ECORP" >}} Anteile zu einem Preis von je {{< bc "52.50" "USD" >}} gebucht. Man
hätte sie genauso gut auch kaufen können, aber ohne eigenes Kapital aufzuwenden
sind sie nun im Besitz des Arbeitnehmers.

### Verkauf

Netterweise haben sich der Arbeitgeber und Broker darauf verständigt, ein
"sell-to-cover" vorzunehmen um die entstandenen Schulden zu begleichen. Dies ist
ein Verkauf!

```beancount
2022-03-16 * "RSU sell-to-cover"
  Assets:US:MyBroker:ECORP                -147 ECORP { } @ 55 USD
    lot: "RSU-4241"
  Expenses:Financial:Fees                 0.10 USD ;; SEC Fee
  Liabilities:US:MyBroker:TaxAndStuff  8032.50 USD
  Assets:US:MyBroker:Cash                52.40 USD
  Income:US:MyBroker:ProfitsAndLosses  ;; calculated automatically

2022-03-17 balance Liabilities:US:MyBroker:TaxAndStuff         0 USD
2022-03-17 balance Assets:US:MyBroker:Cash                 52.40 USD
2022-03-17 balance Income:US:MyBroker:ProfitsAndLosses   -367.50 USD
```

Es wurden {{< bc "147" "ECORP" >}} zu je {{< bc "55.00" "USD" >}} verkauft.
Insgesamt werden also {{< bc "8085" "USD" >}} eingenommen. Davon sind {{< bc
"0.10" "USD" >}} als Gebühr an die SEC gegangen.

Der Rest wurde benutzt um die Schulden zu begleichen. Die Einnahmen aus dem
Verkauf waren leicht höher als die Schulden, die verbleibenden {{< bc "52.40"
"USD" >}} wurden auf das _Cash_ Konto gebucht und könnten ausgezahlt werden.

Ursprünglich wurden die Anteile zu je {{< bc "52.50" "USD" >}} ins Depot
gebucht. Somit ergibt sich ein Gewinn von {{< bc "2.50" "USD" >}} pro Anteil,
insgesamt {{< bc "367.50" "USD" >}}.

Am Ende des beancount Beispiels wurden einige Kontostand-Einträge (_balance_)
vorgenommen. Diese helfen um sicherzugehen, dass wir keine Fehler gemacht haben.

{{% side_note "Konten und Lots - Wie FIFO modellieren?" %}}

Unter der Annahme, dass man Mitarbeiteranteile aus verschiedenen Quellen erhält,
zum Beispiel durch mehrere Grants, stehen wir vor einem Problem: Wie modelliert
man dies in beancount?

Die naheliegende Idee wären ja ein Konto für jeden Grant:

```beancount
;; Account for RSU grant
2022-02-01 open Assets:US:MyBroker:ECORP:RSU-4241
;; Account for ESPP
2022-02-01 open Assets:US:MyBroker:ECORP:ESPP
```

Nach deutschem Steuerrecht jedoch werden alle Aktien in einem Depot
gleichbehandelt. Wenn man mit mehreren Konten modelliert, kann man die
verschiedenen Chargen (Fachsprech "lots") nicht aggregieren. Es macht keinen
Unterschied, ob der US-Broker es vielleicht ermöglicht auszuwählen, was wir
verkaufen. Für das Finanzamt ist das FIFO-Prinzip entscheidet. Die Aktien, die
wir zuerst erwerben, werden zuerst veräußert.

Eine zweite Idee wäre nur einen Account zu nutzen und die Transaktionen zu
taggen:

```beancount
2022-03-15 * "RSU Vesting March" #RSU-4241
  Assets:US:MyBroker:ECORP       300 ECORP { 52.50 USD }
  Income:US:ECORP:PecAdv:RSU  -15750 USD
```

Das hat leider den Nachteil, dass eine einzelne Transaktion mehrere Positionen
aus verschiedenen Quellen haben könnte. Zum Beispiel ein Verkauf von Anteilen
aus RSU- und ESPP-Lots (wenn der Broker es erlaubt).

Die hier im Artikel nutzt beancounts flexibles Metadatensystem, welches
arbiträre `key: value` Einträge erlaubt:

```beancount
2022-09-15 * "Sale of a few shares"
  Assets:US:MyBroker:ECORP        -10 ECORP { } @ 50 USD
    lot: "RSU-4241"
  Assets:US:MyBroker:ECORP        -20 ECORP { } @ 50 USD
    lot: "ESPP"
  Income:US:MyBroker:Profits       ;; calculated
```

Dies erlaubt es uns alle Transaktionen auf ein Depot zu buchen. Die Metadaten
können wir dann in Berichten nutzen, um zu überprüfen, ob unser Kassenbuch mit
den Berichten des Brokers übereinstimmt. Mehr dazu dann in [Teil 4][part4] der
Serie.

All dies zeigt die Flexibilität des Systems und dass man es gegebenenfalls auf
andere länderspezifische Situationen anpassen kann.

{{% /side_note %}}

## Zusammenfassung

In diesem Artikel wurde möglichst kurz beschrieben, wie man Grant, Vesting und
Kauf modelliert von Restricted Stock Units modelliert. Ich nehme gern
[Feedback zum Thema entgegen][issues]. Ansonsten gern mit [Teil 2][part2]
weitermachen.

[beancount]: https://beancount.github.io/docs/
[svji]: https://svij.org/blog/2022/09/16/versteuerung-von-us-aktienoptionen-und-paketen-in-deutschland/
[decm]: https://beancount.github.io/docs/the_double_entry_counting_method.html
[issues]: https://gitlab.com/leipert-projects/homepage/-/issues

[part1]: {{% relref path="2023-01-09_buchhaltung-rsu-espp.md" %}}

[part2]: {{% relref path="2023-01-17_buchhaltung-espp.md" %}}
