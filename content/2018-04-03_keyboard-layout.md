---
slug: 'keyboard-layout'
title: "MacOS ⌨️ Layout"
subtitle: <h2>US International w/o dead keys</h2>
date: 2018-04-03T18:46:01+02:00
language: en
expose: true
summary: "I created a programmer- & umlaut-friendly US International keyboard layout for
MacOS without dead keys"
---

I created a programmer- & umlaut-friendly US International keyboard layout for
MacOS without dead keys.
[\[Download Layout\]](/keyboard-layout/USIntWithoutDeadKeys.keylayout)

Please remember, I am from Germany, so the layout mostly fits my needs and
probably those who want to write Spanish, Danish, Swedish or Finnish.
“Teuto-Scandinavian vivo-programming layout” would probably be a more fitting
name.

![Preview of Keyboard Layout (opens in new
tab)](/keyboard-layout/USIntWithoutDeadKeys.png)

## Requirements

I have the following requirements for a keyboard layout:

1. US Layout for quick (`[{}]`) bracket access
2. Quick umlaut (`äöüß`) access for writing German
3. no dead keys (`` `~^ ``), because those are annoying
4. work properly on ANSI and ISO keyboards, as I use both

The standard MacOS US-International keyboard does not meet these requirements.
It does contain dead keys. No quick access to umlauts is given, instead it
exposes eccentric symbols like the sum sign `∑`. Furthermore on ISO-Keyboards
(e.g. German Macs) the key left of `1` is mapped to `§/±` instead of `` `/~ ``.

## Implementation details

During the implementation I looked at the
[US-International keyboard layout on
Wikipedia](https://commons.wikimedia.org/wiki/File:KB_US-International.svg), but
made some adjustments.

![Normal / ⇧ plane](/keyboard-layout/USIntWithoutDeadKeys-normal.png)

The only adjustment made in the normal plane is the key left to the `1`. It is
now properly mapped to `` `/~ ``. Please note that the key left of `Z` has the
same mapping. This is necessary for proper ANSI/ISO interoperability.

![⌥ / ⌥⇧ plane](/keyboard-layout/USIntWithoutDeadKeys-option.png)

This is the plane that makes the Europeans happy. A lot of people should find
their accented characters and umlauts. The main differences to the "official
keyboard":

1. `⌥+⇧+X` maps to the big eszett `ẞ` instead of the section sign
2. `⌥+(⇧)+X` maps to `§/±` instead of nothing
3. `⌥+R` maps to `ë` instead of `®`
4. `⌥+V` maps to “the rights reserved” sign `®` (memory hook: right of `©`)
5. `⌥+J` and `⌥+K` map to `ï` and `œ`
6. `⌥+(⇧)+-` maps to en-dash/em-dash (&ndash;/&mdash;) instead of the Yen sign

## Conclusion

The only caveat I see so far is that accent circonflexe and accent grave are not
supported, which does not hurt me, as I do not write that much French. If I ever
need to type another character based on a vowel, I just press the vowel longer
to open the MacOS “vowel selection”.

If you want me to adjust a mapping, write
[an issue](https://gitlab.com/leipert-projects/homepage/-/issues), otherwise
feel free to fine-tune the mapping yourself with the help of
[Ukulele](http://scripts.sil.org/ukelele).
