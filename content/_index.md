---
date: 2020-05-16T11:45:35+02:00
language: 'en'
---

Working at [GitLab](https://about.gitlab.com) as a Staff Frontend Engineer.

**Social**

{{< social >}}

**Written things**

I am no blogger by any means, but sometimes I write things.

{{< written_things >}}

**Talks**

Some time ago, I was a little more interested in given public talks, so this
list is probably not growing that much:

{{< talks >}}
