---
slug: buchhaltung-teil-2-espp
title: "Buchhaltung Mitarbeiteranteile"
subtitle: '<h2><small>Teil 2: Employee Stock Purchase Program</small></h2>'
date: 2023-01-16T23:12:22Z
language: de
expose: true
summary: "Teil 2: Employee Stock Purchase Program"
---

{{< accounting/disclaimer >}}

## Einleitung {.startofdoc}

{{< toc >}}

Dies ist Teil zwei einer Artikelserie. Es empfiehlt sich [Teil 1][part1] gelesen
zu haben, bevor man hier weiter liest. Hier ist ein Überblick über alle Teile:

{{% snippet "snippets/buchhaltung_series.md" %}}

## Employee Stock Purchase Program

Ein Employee Stock Purchase Program (ESPP) ist ein freiwilliges Angebot für den
Arbeitnehmer. Im Gegensatz zu RSUs, ist dies nicht Teil der normalen Vergütung,
sondern der Arbeitnehmer nimmt freilwillig teil.

Es gibt verschiedene Ausgestaltungen von solchen Programmen. Eine Variante ist
im Endeffekt ein Sparplan. Die Firma behält jeden Monate einen Prozentsatz des
Gehalts ein. In bestimmten Intervallen werden dann vom angesparten Geld
Firmenanteile gekauft. Auf den Kaufpreis diesers gibt es dann eventuell noch
weitere Rabatte.

Bei diesem ESPP-Modell gibt es drei Events von Bedeutung:

1. _Sparphase_: Bevor Anteile gekauft werden können, muss man erst einmal Geld
   ansparen.
2. _Kauf_: Zu einem bestimmten Tag, "Purchase Date", werden Anteile vom
   angesparten Geld gekauft.
3. _Verkauf_: Anteile werden für Steuerschuld automatisch, oder zur Realisierung
   von Gewinnen manuell, verkauft.

Im folgenden Beispiel spart der Mitarbeiter jeden Monat 10 Prozent seines
Brutto-Gehaltes. Alles sechs Monate werden vom angesparten Geld Firmenanteile
erworben. Die Anteile werden am Kauftag zu 85 Prozent des Marktpreises erworben,
daher: Der Arbeitgeber räumt 15 Prozent Rabatt ein.

### Sparphase

Die Sparphase ist einfach zu modellieren:

```beancount
2022-01-28 * "Salary"
  Income:DE:ECORP:Salary         -8885.00 EUR
  Assets:US:ECORP:ESPP:Savings     888.50 EUR
  Assets:DE:MyBank:Cash           5359.88 EUR
  Expenses:Duties22:TaxNStuff
```

Im Beispiel hat der Mitarbeiter ein Brutto-Lohn von {{< bc 8885 EUR >}}. Auf dem
Konto gehen rund {{< bc 5360 EUR >}} ein und es wurden {{< bc "888.50" EUR >}}
in den ESPP-Sparplan eingezahlt. Die Sparrate wird aufgrund des Bruttolohnes
berechnet, weil diese bei Mitarbeitern mit dem selben Gehalt gleich ist, während
unterschiedliche Voraussetzungen wie Steuerklasse, Kirchenzugehörigkeit oder
Kinderanzahl einen Einfluss auf den Nettolohn haben.

Die genaue Aufschlüsselung von Steuern und weiteren Abgaben ist für das ESPP
nicht so wichtig, daher gehen alle Abgaben auf nur ein Ausgabenkonto. Man kann
dies natürlich auch differenzierter ausmodellieren.

{{% side_note "Differenzierteres Modell für Gehaltseingänge" %}}

Hier ein Beispiel für ein differenzierteres Modell:

```beancount
2022-02-28 * "Salary"
  Income:DE:ECORP:Salary             -8885.00 EUR
  Expenses:Duties22:Tax:Lohnsteuer    1445.33 EUR
  Expenses:Duties22:RentenV            627.75 EUR
  Expenses:Duties22:KrankenV           391.84 EUR
  Expenses:Duties22:ArbLosV                81 EUR
  Expenses:Duties22:PflegeV             90.70 EUR
  Assets:US:ECORP:ESPP:Savings         888.50 EUR
  Assets:DE:MyBank:Cash               5359.88 EUR
```

Persönlich finde ich es interessant, wie viel man über das Jahr für welche
Abgabe zahlt. Außerdem lässt sich so schnell prüfen, ob Gehaltszettel korrekte
Gesamtabrechnungen haben.

Der Vollständigkeit halber hier mal ein Beispiel, welches den Geldwerten Vorteil
aus [Teil 1][rsu-vesting] berücksichtigt:

```beancount
2022-04-28 * "Salary"
    ;; Pec. Advantage RSU in EUR
  Income:DE:ECORP:PecAdv                -14000 EUR
    ;; Pec. Advantage RSU in USD, counter entry for above
  Income:US:ECORP:PecAdvantage:RSU       15750 USD @@ 14000 EUR
    ;; Tax on Pec. Advantage
  Expenses:Duties22:Tax:Lohnsteuer     5740.00 EUR
    ;; Money held back to cover tax
  Assets:US:ECORP:RSU:TaxWithholding  -8032.50 USD @@ 7140 EUR
    ;; Other entries as above
  Income:DE:ECORP:Salary              -8885.00 EUR
  Expenses:Duties22:Tax:Lohnsteuer     1445.33 EUR
  Expenses:Duties22:RentenV             627.75 EUR
  Expenses:Duties22:KrankenV            391.84 EUR
  Expenses:Duties22:ArbLosV                 81 EUR
  Expenses:Duties22:PflegeV              90.70 EUR
  Assets:US:ECORP:ESPP:Savings          888.50 EUR
    ;; Cash income increased by 1400 EUR
    ;; Difference between what was held back and what we owed
  Assets:DE:MyBank:Cash                6759.88 EUR

2022-04-29 balance Income:US:ECORP:PecAdvantage:RSU 0 USD
2022-04-29 balance Assets:US:ECORP:RSU:TaxWithholding 0 USD
```

Das mag sehr komplex aussehen, aber nach einem Jahr Nutzung von beancount: Von
Monat zu Monat kopiert man die Einträge und vergleicht sie mit der
Gehaltsabrechnung. Der Aufwand ist bei Modellierung von lediglich RSU, ESPP,
Verkäufen und Gehalt circa 15 Minuten alle drei Monate. {{%/ side_note %}}

### Kauf

Sechs Monate fleißig gespart, insgesamt {{< bc 5331 EUR>}}, und nun ist es Zeit
für den Kauf von Anteilen:

```beancount
2022-06-30 * "ESPP"
  Assets:US:ECORP:ESPP:Savings              -5331 EUR @@ 5508.75 USD
  Income:US:ECORP:PecAdvantage:ESPP       -971.25 USD
  Assets:US:MyBroker:ECORP                    175 ECORP {37.00 USD}
    lot: "ESPP-2022-06"
  Assets:US:ECORP:ESPP:Savings                  5 USD
  Assets:US:ECORP:ESPP:TaxWithholding      503.11 USD
  Liabilities:US:MyBroker:TaxAndStuff     -503.11 USD

2022-07-01 balance Assets:US:ECORP:ESPP:Savings 0 EUR
2022-07-01 balance Assets:US:ECORP:ESPP:Savings 5 USD
```

Zur Zeit des Kaufs sind umgerechnet {{< bc "5508.75" USD>}} verfügbar. Der
Marktpreis liegt bei {{< bc 37 USD >}} pro Anteil. Der Arbeitgeber billigt wie
bereits beschrieben 15 Prozent Rabatt, daher gilt:

```
       Anteile = ⌊(Ersparnis / (37 USD * 0.85)⌋
     Gegenwert = Anteile * 37 USD
 Geldw Vorteil = 0.15 * Gegenwert
Rest Ersparnis = Ersparnis - (0.85 * Gegenwert)
```

Also in unserem Beispiel:

```
       Anteile = ⌊(5508.75 USD / (0.85 * 37 USD)⌋ = 175
     Gegenwert =                   175 * 37 USD   = 6475 USD
 Geldw Vorteil =                0.15 * 6475 USD   = 971.25 USD
Rest Ersparnis = 5508.75 USD - (0.85 * 6475 USD)  = 5 USD
```

Mit einem Geldwerten Vorteil von {{< bc "971.25" USD>}} wurden {{< bc 175
ECORP>}} Anteile zu einem Gesamtwert von {{< bc 6475 USD>}} erworben. Vom
Ersparnis sind {{< bc 5 USD>}} übrig als "Rest", die beim nächsten ESPP-Kauf
mitverwendet werden können.

Wie auch bei RSUs behält der Broker im Namen des Arbeitgebers eine Summe ein,
hier {{< bc "503.11" USD >}}, um die Steuerschuld des Geldwerten Vorteils zu
begleichen, .

### Verkauf

[Wie bei den RSUs][verkauf] wird auch hier ein "sell-to-cover" vorgenommen, um
die Schuld zu begleichen. Allerdings liefen die Dinge leicht anders:

```beancount
2022-07-03 * "ESPP sell-to-cover"
  Assets:US:MyBroker:ECORP                -12 ECORP { } @ 41.10 USD
    lot: "ESPP-2022-06"
  Expenses:Financial:Fees                0.02 USD ;; SEC
  Liabilities:US:MyBroker:TaxAndStuff  492.08 USD
  Income:US:MyBroker:ProfitsAndLosses    ;; calculated

2022-07-03 * "ESPP sell-to-cover"
  Assets:US:MyBroker:ECORP                 -1 ECORP { } @ 41.04 USD
    lot: "ESPP-2022-06"
  Expenses:Financial:Fees                0.01 USD ;; SEC
  Liabilities:US:MyBroker:TaxAndStuff   41.03 USD
  Income:US:MyBroker:ProfitsAndLosses    ;; calculated

2022-07-03 * "Move overpaid liabilities to cash"
  Liabilities:US:MyBroker:TaxAndStuff  -30.00 USD
  Assets:US:MyBroker:Cash

2022-07-04 balance Liabilities:US:MyBroker:TaxAndStuff       0 USD
2022-07-04 balance Income:US:MyBroker:ProfitsAndLosses -218.14 USD
```

Es wurden beim "sell-to-cover" zu wenig Anteile verkauft um die Kosten zu
decken. Daher wurde ein weiterer Anteil automatisch verkauft. Das Ganze passiert
mit einem {{< bc 30 USD >}} Überschuss, der auf das "Cash"-Konto des Brokers
gebucht wurde.

In unserem Modell, welches FIFO abbildet, werden sowohl die ESPP- als auch die
RSU-Anteile auf das selbe Konto gebucht. Daher wurden hier nicht die beim ESPP
erworbenenen Anteile verkauft, sondern Anteile, die bereits auf dem Konto waren.

Zur Erinnerung: Beim RSU Vesting lag der Kurs bei {{< bc "52.50" USD >}} je
Anteil. Daher hat das "sell-to-cover" hier zu einem Verlust von {{< bc "11.40"
USD>}} respektive {{< bc "11.46" USD >}} pro Anteil geführt. Das klingt schlimm,
aber der Verlust wird mit Gewinnen vom RSU-Verkauf verrechnet, sodass zu diesem
Zeitpunkt der Gesamtgewinn aus Aktienverkäufen nicht mehr {{< bc "367.50"
USD>}}, sondern "nur" noch {{< bc "218.14" USD >}} ist.

{{% side_note "Zusammenfassen von Transaktionen" %}}

Mir hilft es Transaktionen möglichst explizit aufzuschreiben, um zwischen Kauf
und "sell-to-cover" Transaktionen zu trennen. Es gibt auch separate Dokumente
für beide. Allerdings könnte man die Transaktionen zusammenfassen:

```beancount
2022-06-30 * "ESPP and sell to cover"
  Assets:US:ECORP:ESPP:Savings         -5331 EUR @@ 5508.75 USD
  Income:US:ECORP:PecAdvantage:ESPP  -971.25 USD
  Assets:US:MyBroker:ECORP               175 ECORP {37.00 USD}
    lot: "ESPP-2022-06"
  Assets:US:MyBroker:ECORP               -13 ECORP { } @ 41.0953 USD
    lot: "ESPP-2022-06"
  Assets:US:ECORP:ESPP:Savings             5 USD
  Assets:US:ECORP:ESPP:TaxWithholding 503.11 USD
  Expenses:Financial:Fees               0.03 USD ;; SEC
  Assets:US:MyBroker:Cash                 30 USD
  Income:US:MyBroker:ProfitsAndLosses    ;; calculated

2022-07-01 balance Assets:US:ECORP:ESPP:Savings              0 EUR
2022-07-01 balance Assets:US:ECORP:ESPP:Savings              5 USD
2022-07-01 balance Liabilities:US:MyBroker:TaxAndStuff       0 USD
2022-07-01 balance Income:US:MyBroker:ProfitsAndLosses -218.14 USD
```

Obwohl diese Modellierung viel kompakter ist, verliert man ein Informationen:

- Der Verkaufspreis ist der Durchschnitt aus beiden Verkäufen
- Eigentlich passieren Kauf und Verkauf an verschiedenene Tagen. Dies ist jedoch
  unerheblich, solang kein weiterer Verkauf zwischen beiden Tagen stattfand.

Ich würde jedoch die längere Schreibweise empfehlen, und vor allem darauf achten
wie der Broker die Verkäufe in Monats, Quartals- oder Jahresabrechnungen
darstellt, da diese ja im Zweifel als Nachweise dienen.

{{%/ side_note %}}

## Zusammenfassung

Ich hoffe die Modellierung von ESPP ist verständlich. Bitte
[Feedback schreiben][issues], weiter zu [Teil 3: Stock Options][part3], oder
gleich [Teil 4: Berichte erstellen][part4] lesen.

[beancount]: https://beancount.github.io/docs/
[issues]: https://gitlab.com/leipert-projects/homepage/-/issues

[rsu-vesting]: {{% relref path="2023-01-09_buchhaltung-rsu-espp.md#vesting" %}}

[verkauf]: {{% relref path="2023-01-09_buchhaltung-rsu-espp.md#verkauf" %}}

[part1]: {{% relref path="2023-01-09_buchhaltung-rsu-espp.md" %}}

[part2]: {{% relref path="2023-01-17_buchhaltung-espp.md" %}}
