- [Teil 1][part1]: Restricted Stock Units (RSUs)
- [Teil 2][part2]: Employee Stock Purchase Program (ESPP). Wer nicht mitmacht,
  gerne überspringen
- [Teil 3][part3]: Stock Options. Wer diese nicht hat, gerne überspringen.
- [Teil 4][part4]: Berichte erstellen (Rendite berechnen und Jahresbilanz für
  Steuern)

[part1]: {{% relref path="2023-01-09_buchhaltung-rsu-espp.md" %}}

[part2]: {{% relref path="2023-01-17_buchhaltung-espp.md" %}}
